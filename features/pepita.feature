# language: es

# Característica: Paga el boleto del colectivo

# Ejemplo: La pasajera paga con crédito en su tarjeta SUBE
#   Dado una pasajera que tiene un saldo de $300 en su tarjeta SUBE
#   Cuando ella paga el boleto de $100 con su tarjeta SUBE
#   Entonces se emite el boleto
#   Y el saldo en la tarjeta SUBE es $200

Característica:
