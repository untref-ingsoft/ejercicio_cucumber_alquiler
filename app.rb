require_relative './model/alquiler_por_hora'
require_relative './model/alquiler_por_dia'
require_relative './model/alquiler_por_km'
require_relative './model/cliente'
require 'date'

# Comprobar que se hayan proporcionado todos los argumentos necesarios
unless ARGV.length == 5
  puts 'Uso: ruby app.rb <fecha_alquiler> <fecha_devolucion> <cuit> <tipo_alquiler> <parametros_alquiler>'
  exit(1)
end

# Comprobar que las fechas sean válidas
begin
  fecha_alquiler = Date.strptime(ARGV[0], '%Y%m%d')
  fecha_devolucion = Date.strptime(ARGV[1], '%Y%m%d')
rescue ArgumentError
  puts 'Por favor, introduce fechas válidas en formato YYYYMMDD.'
  exit(1)
end

# Comprobar que la fecha de devolución sea posterior o igual a la fecha de alquiler
if fecha_devolucion < fecha_alquiler
  puts 'La fecha de devolución no puede ser anterior a la fecha de alquiler.'
  exit(1)
end

# Comprobar longitud y tipo de CUIT
unless ARGV[2].match(/^\d{11}$/)
  puts 'El CUIT no es válido. Debe estar compuesto por 11 dígitos numéricos.'
  exit(1)
end

cliente = Cliente.new(ARGV[2])
tipo_alquiler = ARGV[3].downcase
params_alquiler = ARGV[4].to_i

# Compropbar que params_alquiler sea un número positivo
if params_alquiler.negative?
  puts 'Por favor, introduce un número positivo para los parámetros de alquiler.'
  exit(1)
end

alquiler = case tipo_alquiler
           when 'h'
             AlquilerPorHora.new(params_alquiler, cliente, fecha_alquiler, fecha_devolucion)
           when 'd'
             AlquilerPorDia.new(params_alquiler, cliente, fecha_alquiler, fecha_devolucion)
           when 'k'
             AlquilerPorKm.new(params_alquiler, cliente, fecha_alquiler, fecha_devolucion)
           else
             puts "Tipo de alquiler no reconocido: #{tipo_alquiler}"
             exit(1)
           end

puts "importe:#{alquiler.importe.round(1)} & ganancia:#{alquiler.ganancia.round(1)}"
