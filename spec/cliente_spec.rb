require 'spec_helper'

describe 'Cliente' do
    
    it 'empresa? es false cuando cuit es 20123456789' do
        expect(Cliente.new("20123456789").empresa?).to eq false
    end

    it 'empresa? es true cuando cuit es 26123456789' do
      expect(Cliente.new("20123456789").empresa?).to eq false
  end
end
