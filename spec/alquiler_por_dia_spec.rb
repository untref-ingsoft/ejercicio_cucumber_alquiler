require 'spec_helper'

describe 'AlquilerPorDia' do
    
  cliente_particular = Cliente.new('20123456789')
  cliente_empresa = Cliente.new('26123456789')
  fecha_alquiler = Date.strptime('20230801', '%Y%m%d')

  it 'importe 2001.0 cuando alquilo por 1 dia' do
    fecha_devolucion = fecha_alquiler + 1
    expect(AlquilerPorDia.new(1, cliente_particular, fecha_alquiler, fecha_devolucion).importe).to eq 2001.0
  end

  it 'importe 4002.0 cuando alquilo por 2 dias' do
    fecha_devolucion = fecha_alquiler + 2
    expect(AlquilerPorDia.new(2, cliente_particular, fecha_alquiler, fecha_devolucion).importe).to eq 4002.0
  end

  it 'ganancia por un dia debe ser 900.5' do
    fecha_devolucion = fecha_alquiler + 1
    expect(AlquilerPorDia.new(1, cliente_particular, fecha_alquiler, fecha_devolucion).ganancia.round(1)).to eq 900.5
  end
  
  it 'importe 1900.95 cuando alquilo por 1 dia y es empresa' do
    fecha_devolucion = fecha_alquiler + 1
    expect(AlquilerPorDia.new(1, cliente_empresa, fecha_alquiler, fecha_devolucion).importe).to eq 1900.95
  end

  it 'importe 3801.9 cuando alquilo por 2 dias y es empresa' do
    fecha_devolucion = fecha_alquiler + 2
    expect(AlquilerPorDia.new(2, cliente_empresa, fecha_alquiler, fecha_devolucion).importe).to eq 3801.9
  end

  it 'ganancia por un dia debe ser 855.4' do
    fecha_devolucion = fecha_alquiler + 1
    expect(AlquilerPorDia.new(1, cliente_empresa, fecha_alquiler, fecha_devolucion).ganancia.round(1)).to eq 855.4
  end

  it 'importe 8004.0 cuando alquilo por 1 dia y lo devuelvo un día tarde' do
    fecha_devolucion = fecha_alquiler + 2
    expect(AlquilerPorDia.new(1, cliente_particular, fecha_alquiler, fecha_devolucion).importe).to eq 8004.0
  end  

  it 'ganancia 3601.8 cuando alquilo por 1 dia y lo devuelvo un día tarde' do
    fecha_devolucion = fecha_alquiler + 2
    expect(AlquilerPorDia.new(1, cliente_particular, fecha_alquiler, fecha_devolucion).ganancia).to eq 3601.8
  end  

  it 'importe 7603.8 cuando alquilo por 1 dia y lo devuelvo un día tarde siendo empresa' do
    fecha_devolucion = fecha_alquiler + 2
    expect(AlquilerPorDia.new(1, cliente_empresa, fecha_alquiler, fecha_devolucion).importe).to eq 7603.8
  end  

  it 'ganancia 3421.7 cuando alquilo por 1 dia y lo devuelvo un día tarde siendo empresa' do
    fecha_devolucion = fecha_alquiler + 2
    expect(AlquilerPorDia.new(1, cliente_empresa, fecha_alquiler, fecha_devolucion).ganancia.round(1)).to eq 3421.7
  end  
end
