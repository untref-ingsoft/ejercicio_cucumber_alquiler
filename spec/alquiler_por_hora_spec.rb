require 'spec_helper'

describe 'AlquilerPorHora' do

  cliente_particular = Cliente.new('20123456789')
  cliente_empresa = Cliente.new('26123456789')
  fecha_alquiler = Date.strptime('20230801', '%Y%m%d')

  it 'importe 0 cuando horas 0' do
    expect(AlquilerPorHora.new(0, cliente_particular, fecha_alquiler, fecha_alquiler).importe).to eq 0
  end

  it 'importe 99 cuando hora 1' do
    expect(AlquilerPorHora.new(1, cliente_particular, fecha_alquiler, fecha_alquiler).importe).to eq 99
  end

  it 'ganancia 0 cuando horas 0' do
    expect(AlquilerPorHora.new(0, cliente_particular, fecha_alquiler, fecha_alquiler).ganancia).to eq 0
  end

  it 'ganancia 44.55 cuando horas 1' do
    expect(AlquilerPorHora.new(1, cliente_particular, fecha_alquiler, fecha_alquiler).ganancia.round(2)).to eq 44.55
  end

  it 'importe 0 cuando horas 0 y es una empresa' do
    expect(AlquilerPorHora.new(0, cliente_empresa, fecha_alquiler, fecha_alquiler).importe).to eq 0
  end

  it 'importe 94.0 cuando hora 1 y es una empresa' do
    expect(AlquilerPorHora.new(1, cliente_empresa, fecha_alquiler, fecha_alquiler).importe.round(0)).to eq 94.0
  end

  it 'ganancia 0 cuando horas 0 y es empresa' do
    expect(AlquilerPorHora.new(0, cliente_empresa, fecha_alquiler, fecha_alquiler).ganancia).to eq 0
  end

  it 'ganancia 42.32 cuando horas 1 y es empresa' do
    expect(AlquilerPorHora.new(1, cliente_empresa, fecha_alquiler, fecha_alquiler).ganancia.round(2)).to eq 42.32
  end
end
