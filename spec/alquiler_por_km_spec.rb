require 'spec_helper'

describe 'AlquilerPorKm' do
    
  cliente_particular = Cliente.new('20123456789')
  cliente_empresa = Cliente.new('26123456789')
  fecha_alquiler = Date.strptime('20230801', '%Y%m%d')

  it 'importe 100 cuando alquilo por 0 Km' do
      expect(AlquilerPorKm.new(0, cliente_particular, fecha_alquiler, fecha_alquiler).importe).to eq 100.0
  end

  it 'importe 110 cuando alquilo por 1 Km' do
    expect(AlquilerPorKm.new(1, cliente_particular, fecha_alquiler, fecha_alquiler).importe).to eq 110.0
  end

  it 'importe 180 cuando alquilo por 8 Km' do
    expect(AlquilerPorKm.new(8, cliente_particular, fecha_alquiler, fecha_alquiler).importe).to eq 180.0
  end

  it 'ganancia 49.5 cuando alquilo por 1 Km' do
    expect(AlquilerPorKm.new(1, cliente_particular, fecha_alquiler, fecha_alquiler).ganancia).to eq 49.5
  end

  it 'ganancia 81 cuando alquilo por 8 Km' do
    expect(AlquilerPorKm.new(8, cliente_particular, fecha_alquiler, fecha_alquiler).ganancia).to eq 81.0
  end

  it 'importe 95 cuando alquilo por 0 Km y es empresa' do
    expect(AlquilerPorKm.new(0, cliente_empresa, fecha_alquiler, fecha_alquiler).importe).to eq 95.0
  end

  it 'importe 104.5 cuando alquilo por 1 Km y es empresa' do
    expect(AlquilerPorKm.new(1, cliente_empresa, fecha_alquiler, fecha_alquiler).importe).to eq 104.5
  end

  it 'importe 171 cuando alquilo por 8 Km y es empresa' do
    expect(AlquilerPorKm.new(8, cliente_empresa, fecha_alquiler, fecha_alquiler).importe).to eq 171.0
  end

  it 'ganancia 47.0 cuando alquilo por 1 Km y es empresa' do
    expect(AlquilerPorKm.new(1, cliente_empresa, fecha_alquiler, fecha_alquiler).ganancia.round(1)).to eq 47.0
  end

  it 'ganancia 77.0 cuando alquilo por 8 Km y es empresa' do
    expect(AlquilerPorKm.new(8, cliente_empresa, fecha_alquiler, fecha_alquiler).ganancia.round(1)).to eq 77.0
  end

  it 'importe 220.0 cuando alquilo por 1 Km y devuelvo con retraso' do
    fecha_devolucion = fecha_alquiler + 1
    expect(AlquilerPorKm.new(1, cliente_particular, fecha_alquiler, fecha_devolucion).importe).to eq 220.0
  end

  it 'ganancia 99.0 cuando alquilo por 1 Km y devuelvo con retraso' do
    fecha_devolucion = fecha_alquiler + 1
    expect(AlquilerPorKm.new(1, cliente_particular, fecha_alquiler, fecha_devolucion).ganancia).to eq 99.0
  end

  it 'importe 209.0 cuando alquilo por 1 Km y devuelvo con retraso y es empresa' do
    fecha_devolucion = fecha_alquiler + 1
    expect(AlquilerPorKm.new(1, cliente_empresa, fecha_alquiler, fecha_devolucion).importe).to eq 209.0
  end

  it 'ganancia 94.05 cuando alquilo por 1 Km y devuelvo con retraso y es empresa' do
    fecha_devolucion = fecha_alquiler + 1
    expect(AlquilerPorKm.new(1, cliente_empresa, fecha_alquiler, fecha_devolucion).ganancia).to eq 94.05
  end
end
