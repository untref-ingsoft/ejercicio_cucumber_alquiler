require 'spec_helper'

describe 'App' do

  it 'importe cero cuando alquilo cero horas' do
    resultado = `ruby app.rb 20230815 20230815 20299998886 h 0`
    expect(resultado.strip).to eq 'importe:0.0 & ganancia:0.0'
  end

  it 'importe 99 cuando alquilo una hora' do
    resultado = `ruby app.rb 20230815 20230815 20299998886 h 1`
    expect(resultado.strip).to eq 'importe:99.0 & ganancia:44.6'
  end

  it 'importe 0.0 y ganancia 0.0 cuando alquilo por 0 dias' do
    resultado = `ruby app.rb 20230815 20230815 20299998886 d 0`
    expect(resultado.strip).to eq 'importe:0.0 & ganancia:0.0'
  end

  it 'importe 2001.0 y ganancia 900.5 cuando alquilo por 1 dia' do
    resultado = `ruby app.rb 20230815 20230815 20299998886 d 1`
    expect(resultado.strip).to eq 'importe:2001.0 & ganancia:900.5'
  end

  it 'importe 110.0 y ganancia 49.5 cuando alquilo por 1 km' do
    resultado = `ruby app.rb 20230815 20230815 20299998886 k 1`
    expect(resultado.strip).to eq 'importe:110.0 & ganancia:49.5'
  end

  it 'importe 180.0 y ganancia 81 cuando alquilo por 8 km' do
    resultado = `ruby app.rb 20230815 20230815 20299998886 k 8`
    expect(resultado.strip).to eq 'importe:180.0 & ganancia:81.0'
  end

  it 'importe 94.1 cuando alquilo una hora y es empresa' do
    resultado = `ruby app.rb 20230815 20230815 26299998886 h 1`
    expect(resultado.strip).to eq 'importe:94.1 & ganancia:42.3'
  end

  it 'importe 1901.0 y ganancia 855.4 cuando alquilo por 1 dia y es empresa' do
    resultado = `ruby app.rb 20230815 20230815 26299998886 d 1`
    expect(resultado.strip).to eq 'importe:1901.0 & ganancia:855.4'
  end

  it 'importe 104.5 y ganancia 47.0 cuando alquilo por 1 km y es empresa' do
    resultado = `ruby app.rb 20230815 20230815 26299998886 k 1`
    expect(resultado.strip).to eq 'importe:104.5 & ganancia:47.0'
  end

  it 'importe 8004.0 y ganancia 3601.8 cuando alquilo por 1 dia y lo devuelvo 1 día tarde' do
    resultado = `ruby app.rb 20230815 20230817 20299998886 d 1`
    expect(resultado.strip).to eq 'importe:8004.0 & ganancia:3601.8'
  end

  it 'importe 7603.8 y ganancia 3421.7 cuando alquilo por 1 dia y lo devuelvo 1 día tarde siendo empresa' do
    resultado = `ruby app.rb 20230815 20230817 26299998886 d 1`
    expect(resultado.strip).to eq 'importe:7603.8 & ganancia:3421.7'
  end

  it 'importe 220.0 y ganancia 99.0 cuando alquilo por 1 km y devuelvo con retraso' do
    resultado = `ruby app.rb 20230815 20230816 20299998886 k 1`
    expect(resultado.strip).to eq 'importe:220.0 & ganancia:99.0'
  end

  it 'importe 209.0 y ganancia 94.1 cuando alquilo por 1 km y devuelvo con retraso siendo empresa' do
    resultado = `ruby app.rb 20230815 20230816 26299998886 k 1`
    expect(resultado.strip).to eq 'importe:209.0 & ganancia:94.1'
  end
end
