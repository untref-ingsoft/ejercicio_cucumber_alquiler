require_relative './alquiler'

class AlquilerPorHora < Alquiler
  PRECIO_POR_HORA = 99.0

  def importe_bruto
    PRECIO_POR_HORA * @cantidad
  end
end
