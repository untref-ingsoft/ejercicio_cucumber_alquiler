class AlquilerPorKm < Alquiler
  PRECIO_POR_KM = 10.0
  PRECIO_BASE = 100.0

  def importe_bruto
    PRECIO_BASE + (PRECIO_POR_KM * @cantidad)
  end

  def retraso?
    (@fecha_devolucion - @fecha_alquiler).to_i.positive?
  end
end
