class Alquiler
  PORCENTAJE_GANANCIA = 0.45
  DESCUENTO_EMPRESA = 0.05
  RECARGO_PENALIZACION = 1.0

  def initialize(cantidad, cliente, fecha_alquiler, fecha_devolucion)
    @cantidad = cantidad
    @cliente = cliente
    @fecha_alquiler = fecha_alquiler
    @fecha_devolucion = fecha_devolucion
  end

  def importe
    total = importe_bruto
    total += total * RECARGO_PENALIZACION if retraso?
    total -= total * DESCUENTO_EMPRESA if @cliente.empresa?
    total
  end

  def ganancia
    importe * PORCENTAJE_GANANCIA
  end

  def importe_bruto; end
  def retraso?; end
end
