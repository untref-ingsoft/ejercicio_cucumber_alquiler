class Cliente
  def initialize(cuit)
    @cuit = cuit
  end

  def empresa?
    @cuit.start_with?('26')
  end
end
