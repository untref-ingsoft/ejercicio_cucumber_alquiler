require_relative './alquiler'

class AlquilerPorDia < Alquiler
  PRECIO_POR_DIA = 2001.0

  def importe_bruto
    if retraso?
      PRECIO_POR_DIA * (@fecha_devolucion - @fecha_alquiler).to_i
    else
      PRECIO_POR_DIA * @cantidad
    end
  end

  def retraso?
    (@fecha_devolucion - @fecha_alquiler).to_i > @cantidad
  end
end
